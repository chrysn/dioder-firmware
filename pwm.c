/*
 *  PWM for alternative DIODER firmware
 *  Copyright (C) 2011-2012 chrysn <chrysn@fsfe.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* this is far too tightly coupled to the way the leds are used (single port
 * with nothing that can't stand being written to 0) to be better configurable
 * than directly in the c file */

#define PORTA_RED_BIT 2
#define PORTA_GREEN_BIT 0
#define PORTA_BLUE_BIT 1

#define PORTA_LEDS_MASK(r, g, b) ((((r) != 0) << PORTA_RED_BIT) | (((g) != 0) << PORTA_GREEN_BIT) | (((b) != 0) << PORTA_BLUE_BIT))

volatile uint8_t time; /* incremented by the pwm interrupt, so at around 111Hz */

uint8_t color[3]; /* real color values for r, g, b */
uint8_t fastcolor[8]; /* enabled for 1<<i cycles */

static void fastcolor_fill(uint8_t r, uint8_t g, uint8_t b)
{
	uint8_t i;
	uint8_t current_bit;

	for (i = 0; i < sizeof(fastcolor)/sizeof(fastcolor[0]); ++i)
	{
		current_bit = 1 << i;
		fastcolor[i] = PORTA_LEDS_MASK(r & current_bit, g & current_bit, b & current_bit);
	}
}

/* pwm function for bit-banging pwm. the main idea is to burn about half the
 * cpu cycles in quite a busy loop and stay statically on for the rest of the
 * time. the timing is chosen so that the interrupt timer ticks as rarely as
 * possible, and the loop is slowed down to distribute the pin state changes
 * appropriately over the remaining time. (ticking even rarer would cause
 * flickering, but ticking more often would just increase overhead, and we have
 * to constantly pull the led pins over half the interrupt time anyway.
 * moreover, the longer the cycle length, the finer the details that can be
 * covered by pulling a pin high for just a cpu cycle.)
 *
 * color information is split in a high and a low byte (color and fastcolor).
 *
 * the high byte is treated in a relatively time-consuming loop of about 64
 * cycles per increment, where color % 128 is treated, and in the long skip
 * until the timer interrupt kicks in again.
 *
 * the low byte is treated in an assembler-assisted code section, where static
 * blocks of "enable some leds, nop if necessary, disable all leds" that enable
 * the leds for cycle counts of powers of two.
 *
 *
 * in details: 
 *
 * * a step in the normal color[] fading takes 64 cycles, making 16384 cycles
 *   for fading through the whole bytes (whereof half is spent in the
 *   interrupt). in total, a little overhead adds to that, so even if we say
 *   18k cycles, at 8mhz clock (/4 = 2mhz instruction clock, this gives 111hz,
 *   which is sufficient.
 * * 6 bits of fastcolor are used for 1 to 32 clock cycles
 * * when we finish the interrupt routine, we need to sleep the 8192 cycles
 *   again. with a 32 divisor, the 8bit counter overflows in exactly that many
 *   cycles.
 *
 * */

static void leds_pwm(void) interrupt 0
{
	uint8_t i;
	uint8_t red_threshold, green_threshold, blue_threshold;

	red_threshold = color[0] & 0x7f;
	green_threshold = color[1] & 0x7f;
	blue_threshold = color[2] & 0x7f;

	PORTA = PORTA_LEDS_MASK(red_threshold, green_threshold, blue_threshold); /* don't turn on leds with 0 duration at all */

	/* make everything linear */
	__asm
		nop
		nop
	__endasm;

	i = 1;
	do {
		PORTA ^=
		    PORTA_LEDS_MASK(i == red_threshold,
				    i == green_threshold, i == blue_threshold);
		i++;
		/* could be unrolled in steps of powers of two */

		__asm
			nop
		__endasm;

		PORTA ^=
		    PORTA_LEDS_MASK(i == red_threshold,
				    i == green_threshold, i == blue_threshold);
		i++;

		/* an unrolled step takes 59 cycles, a loop step takes 66
		 * cycles. adding some cycles to get 64 cycles per loop (precise timing has yet to be determined)
		 */
		
	} while (i < 127);
	PORTA ^=
	    PORTA_LEDS_MASK(i == red_threshold,
			    i == green_threshold, i == blue_threshold);
	i++;

	/* i should be 128 now */

	/* FIXME: all LEDs should be off now; check if i'm not off by one
	 * (alternatively, only those that are supposed to be lit 128 cycles
	 * should be left over, and disabled after the loop and appropriate NOOPs) */

	/* shorter pulses
	 *
	 * this works in such a flexible way (more flexible than having
	 * completely static sections with hardcoded patterns) because we
	 * always return to 0 (clrf _PORTA in assembly).
	 *
	 * this cost us a bit more than 64 cpu cycles and equally many
	 * instructions (could i find out how long jumps take, i could just as
	 * well stuff the longer sequences with loops)
	 *
	 * this could be slightly optimized by switching from "clear and set
	 * new" to "pre-load value, and set at the right moment" for everything
	 * but the 1, 2 and 4 cycle parts -- 5 instructions per part could
	 * be saved.
	 * */
	/* 1 cycle */
	PORTA = fastcolor[0];
	__asm
		CLRF _PORTA
	__endasm;
	/* 2 cycles */
	PORTA = fastcolor[1];
	__asm
		nop
		CLRF _PORTA
	__endasm;
	/* 4 cycles */
	PORTA = fastcolor[2];
	__asm
		nop
		nop
		nop
		CLRF _PORTA
	__endasm;
	/* 8 cycles */
	PORTA = fastcolor[3];
	__asm
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		CLRF _PORTA
	__endasm;
	/* 16 cycles */
	PORTA = fastcolor[4];
	__asm
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		CLRF _PORTA
	__endasm;
	/* 32 cycles */
	PORTA = fastcolor[5];
	__asm
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		CLRF _PORTA
	__endasm;

	/* reset timer, start counting again */
	TMR0 = 1;
	/* interrupts were ignored until now; starting again */
	T0IF = 0;

	PORTA = PORTA_LEDS_MASK(color[0] & 0x80, color[1] & 0x80, color[2] & 0x80);

	/* we're not in a hurry now no more -- just take care not to waste so
	 * much time that the main loop can't do its stuff any more */

	time ++;
}

static void porta_output_setup(void)
{
// 16f685, which should be similar and is used for simulation (gpsim can't do
// 16f684), can't and doesn't need to do this
#ifdef __16f684
	// initialization according to example 4-1 in the data sheet
	CMCON0 = 0x07;
#endif
}

static void led_setup(void)
{
	porta_output_setup();

	/* all the mosfet channels are output pins: RA0, RA1, RS2; leave the rest
	 * tri-stated */
	TRISA = 0b11111000;
}

static void timer_setup(void)
{
	/* taken more or less verbatim from http://burningsmell.org/pic16f628/src/0002-interrupt.c */

	T0CS = 0;		// Clear to enable timer mode.
	PSA = 0;		// Clear to assign prescaler to Timer 0.

	PS2 = 1;		// Set up prescaler to 1/32
	PS1 = 0;
	PS0 = 0;

	INTCON = 0;		// Clear interrupt flag bits.
	GIE = 1;		// Enable all interrupts.

	TMR0 = 0;		// Set Timer 0 to 0.  
	T0IE = 1;		// Enable peripheral interrupts.
}

static void pwm_setup(void)
{
	led_setup();

	timer_setup();
}
