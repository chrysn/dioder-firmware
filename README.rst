===========================
Alternative DIODER firmware
===========================

.. warning:: As of 2014, new devices are incompatible, see `device variants`_.

.. _`device variants`: http://christian.amsuess.com/tutorials/threebutton_dioder/#device-variants

This project repository contains an alternative firmware for the IKEA DIODER.

The firmware aims to be a drop-in replacement for the IKEA default firmware,
but provide several additional features:

* Multi-button combinations to
    * fade between particular colors
    * change brightness and saturation
* Support for additional peripherials like
    * reed contacts for open doors or that liek
    * serial interface to more advanced devices

While the basics of operation (dimming, button support) are already
implemented, the firmware is not yet usable as a complete and enhanced
replacement for the stock firmware.

Parts
=====

Groundwork
----------

The basics of the project are described in the `Programming a three-button
DIODER`_ tutorial -- everything from where to obtain a DIODER to how to put new
firmware on it.

.. _`Programming a three-button DIODER`: http://christian.amsuess.com/tutorials/threebutton_dioder/

Pulse width modulation
----------------------

The ``pwm.c`` module contains code to dim the LED lines to a 2^14th of their
brightness at 111Hz, dark enough that you have to look directly at the LED to
notice that it's on at all, but fast enough to avoid flickering when the eye is
moving. This is necessary to avoid the impression of being turned off abruptly
at the end of a fade-out sequence.

On the downside, the PWM code takes about half the chip's CPU time inside an
interrupt handler (about 4ms inside, 4ms outside). This will be a challenge
when communication with something else is implemented.

Please see the comments in ``pwm.c`` for the details of the timing.

High-level logic
----------------

The current ``main.c`` code tries to simulate the original DIODER behavior with
its kind-of-but-not-quite HSV hue fading. It is Not Quite There Yet and likely
to change.

Copyright
=========

© 2011--2012 chrysn <chrysn@fsfe.org>

This project is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
