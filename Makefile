# switch to 16f684 for running and to 16f685 for simulation
PIC = 16f685

PK2CMPATH = /tmp/pickit/pk2cmdv1.20LinuxMacSource/

%.hex %.cod: %.c Makefile *.c
	ln -sf hardware_$(PIC).h hardware.h
	sdcc --debug -mpic14 -p$(PIC) -D__$(PIC) $<

upload: main.hex
	${PK2CMDPATH}pk2cmd -PPIC${PIC} -F$< -M -B${PK2CMDPATH}

main.lxt: main.cod main.stc
	gpsim -s $< -c main.stc

clean:
	rm -f *.cod *.asm *.adb *.hex *.lst *.o *.p hardware.h *.lxt
