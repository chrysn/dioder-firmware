/*
 *  ADC components for the alternative DIODER firmware
 *  Copyright (C) 2011-2012 chrysn <chrysn@fsfe.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

static void adc_start_conversion(void)
{
	ADCON0 |= 0b00000010;
}

static char adc_is_finished(void)
{
	return !(ADCON0 & 0b00000010);
}

static void adc7_setup(void)
{
	ANSEL = (1 << 7);	// use ADC 7 (color dial), disable all others
	ADCON1 = 0b101 << 4;	// use 16 as divider factor (recommended for 4mhz or 8mhz clock)
	ADCON0 = 0b00011101;	// left justified, reference is Vdd, channel used is always 7, enabled and set to go
}
