/*
 *  Alternative DIODER firmware
 *  Copyright (C) 2011-2012 chrysn <chrysn@fsfe.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hardware.h"
#include <stdint.h>

/* Setup chip configuration -- losely based on the 00 0C default configuration*/
typedef unsigned int config;
config at 0x2007 __CONFIG = _FCMEN_OFF & _IESO_OFF &
#ifndef __16f685
    _BOD_OFF &
#endif
    _CPD_OFF &
    _CP_OFF & _MCLRE_OFF & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT;

#include "hardware_real.c"
#include "pwm.c"
#include "adc.c"

static void clock_setup(void)
{
/* for some reasons, the simulation with 16f685 hangs here or doesn't work when waiting in this loop */
#ifdef __16f684
	// wait for HFINTOSC to become stable -- not sure if required
	while ((OSCCON & (0b100)) == 0) ;
#endif
	// 8 mhz
	OSCCON = 0b1111001;
}

static void setup(void)
{
	clock_setup();

	adc7_setup();

	adc_start_conversion();

	pwm_setup();
}

#define NPHASES 5
#define PHASEMAX 65535
#define VALMAX 255
#define PHASEDIV 256

static void set_phase(uint16_t phase)
{
	uint16_t phase_start;

	if (phase < PHASEMAX/NPHASES * 1) {
		phase_start = PHASEMAX/NPHASES*0;

		color[0] = VALMAX;
		color[1] = 0;
		color[2] = (phase - phase_start) * NPHASES / PHASEDIV;
	} else if (phase < PHASEMAX/NPHASES * 2) {
		phase_start = PHASEMAX/NPHASES*1;

		color[0] = VALMAX - (phase - phase_start) * NPHASES / PHASEDIV;
		color[1] = 0;
		color[2] = VALMAX;
	} else if (phase < PHASEMAX/NPHASES * 3) {
		phase_start = PHASEMAX/NPHASES*2;

		color[0] = 0;
		color[1] = (phase - phase_start) * NPHASES / PHASEDIV;
		color[2] = (VALMAX - (phase - phase_start) * NPHASES / PHASEDIV);
	} else if (phase < PHASEMAX/NPHASES * 4) {
		phase_start = PHASEMAX/NPHASES*3;

		color[0] = (phase - phase_start) * NPHASES / PHASEDIV;
		color[1] = VALMAX;
		color[2] = 0;
	} else {
		phase_start = PHASEMAX/NPHASES*4;

		color[0] = VALMAX;
		color[1] = VALMAX;
		color[2] = (phase - phase_start) * NPHASES / PHASEDIV;
	}
}

enum {
	OFF,
	EXPLICIT,
	FADE_UP,
	FADE_DOWN,
	STEP
} mode = OFF;

uint16_t fade_position = 0;
uint16_t last_adc_reading = 0;

static void loop(void)
{
	uint16_t adc_reading;
	if (adc_is_finished()) {
		adc_reading = (ADRESH << 8) + ADRESL;

		if (adc_reading != last_adc_reading) {
			if (mode != OFF)
			{
				mode = EXPLICIT;
				set_phase(-adc_reading);
			}
			last_adc_reading = adc_reading;
		}

		if (mode == FADE_UP)
		{
			set_phase(-fade_position);
			fade_position++;
			if (fade_position == 0)
				mode = FADE_DOWN;
		}
		else if (mode == FADE_DOWN)
		{
			set_phase(fade_position);
			fade_position++;
			if (fade_position == 0)
				mode = FADE_UP;
		}

		adc_start_conversion();
	}

	if (button_left_pressed())
		mode = STEP;
	if (button_center_pressed())
	{
		mode = OFF;
		color[0] = 0;
		color[1] = 0;
		color[2] = 0;
	}
	if (button_right_pressed())
		mode = FADE_UP;
}

void main(void)
{
	setup();

	color[0] = 100;
	color[1] = 200;
	color[2] = 5;
	fastcolor_fill(6, 2, 63);

	while (1) {
//		loop();
	}
}
