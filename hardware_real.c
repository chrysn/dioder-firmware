/*
 *  Hardware abstractions for buttons for the alternative DIODER firmware
 *  Copyright (C) 2011-2012 chrysn <chrysn@fsfe.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BUTTONLEFT_PORT PORTC
#define BUTTONLEFT_BIT 5
#define BUTTONCENTER_PORT PORTA
#define BUTTONCENTER_BIT 3
#define BUTTONRIGHT_PORT PORTA
#define BUTTONRIGHT_BIT 4

static char button_left_pressed(void)
{
	return !(BUTTONLEFT_PORT & (1 << BUTTONLEFT_BIT));
}

static char button_center_pressed(void)
{
	return !(BUTTONCENTER_PORT & (1 << BUTTONCENTER_BIT));
}

static char button_right_pressed(void)
{
	return !(BUTTONRIGHT_PORT & (1 << BUTTONRIGHT_BIT));
}
